clc
clear
syms x1 x2;
%f=100*(x2-x1^2)^2+5*(1-x1)^2;
f=(x1^2+x2-11)^2+(x1+x2^2-7)^2;
fg=matlabFunction(f);
dfdx1=diff(f,x1);
dfdx2=diff(f,x2);

mat_dfdx1=matlabFunction(dfdx1);
mat_dfdx2=matlabFunction(dfdx2);
grad=@(x)[mat_dfdx1(x(1),x(2)),mat_dfdx2(x(1),x(2))];
oldmin=[10;-10];
min=oldmin;

d2fx1=diff(dfdx1,x1);
d2fx2=diff(dfdx2,x2);
d2fx12=diff(dfdx1,x2);
Ges=@(x)[subs(d2fx1,[x1 x2],x),subs(d2fx12,[x1 x2],x);subs(d2fx12,[x1 x2],x),subs(d2fx2,[x1 x2],x)];
H=double(inv(Ges(oldmin')));

lambda=0;
syms lamb;
result=0;
k=0;
E=10^(-6);

while 1
lambda=minFunc2(matlabFunction(subs(f,[x1;x2],oldmin-lamb*H*grad(oldmin)')),0,1);
min=oldmin-lambda*H*grad(oldmin)'

if mod(k+1,2)~=0
gamma=(grad(min)-grad(oldmin))';
delta=min-oldmin;
H=H+((delta-H*gamma)*(delta-H*gamma)')/((delta-H*gamma)'*gamma);
end
if (norm(H)<E) ||(norm(min-oldmin)<E)
result=min;
break
end
k=k+1;
oldmin=min;
H=double(inv(Ges(oldmin')));
end


fprintf('����� ��������: [%d, %d]\n\n',result);
