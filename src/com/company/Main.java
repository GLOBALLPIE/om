package com.company;

import java.util.Arrays;
import java.util.Random;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

public class Main {

    public static double golden(DoubleUnaryOperator f, double a, double b) {
        double PHI = (1 + Math.sqrt(5)) / 2;
        double E = 1e-5;
        double x1, x2;
        while (true) {
            x1 = b - (b - a) / PHI;
            x2 = a + (b - a) / PHI;
            if (f.applyAsDouble(x1) >= f.applyAsDouble(x2))
                a = x1;
            else
                b = x2;
            if (Math.abs(b - a) < E)
                break;
        }
        return (a + b) / 2;
    }

    public static double funcValue(DoubleBinaryOperator f, Point p) {
        return f.applyAsDouble(p.getX1(), p.getX2());
    }


    public static double dfdx1(DoubleBinaryOperator f, Point p) {
        double h = Math.sqrt(1e-7);
        return (f.applyAsDouble(p.getX1() + h, p.getX2()) - f.applyAsDouble(p.getX1() - h, p.getX2())) / (2 * h);
       // return -400*p.getX1()*(-p.getX1()*p.getX1()+p.getX2())+10*p.getX1()-10;
    }

    public static double dfdx2(DoubleBinaryOperator f, Point p) {
        double h = Math.sqrt(1e-7);
        return (f.applyAsDouble(p.getX1(), p.getX2() + h) - f.applyAsDouble(p.getX1(), p.getX2() - h)) / (2 * h);
        //return -200*p.getX1()*p.getX1()+200*p.getX2();
    }

    public static void minSoprGrad(DoubleBinaryOperator f, Point point){
        double E = 1e-5;
        Point grad = new Point(dfdx1(f, point), dfdx2(f, point));
        double lambda;
        int j=0;
        DoubleUnaryOperator fu;
        double w;
        Point S=grad.scale(-1);
        while(true){
            Point finalPoint = point;
            Point finalGrad = grad;
            Point finalS = S;
            fu = (lamb) -> f.applyAsDouble(finalPoint.getX1() + lamb * finalS.getX1(), finalPoint.getX2() + lamb * finalS.getX2());
            lambda = golden(fu, 0, 1);

            point = point.add(S.scale(lambda));
            grad = new Point(dfdx1(f, point), dfdx2(f, point));
            w=(grad.norm()*grad.norm())/(finalGrad.norm()*finalGrad.norm());
            S=grad.scale(-1).add(S.scale(w));
            System.out.println(point);
            //System.out.println(grad);
            if(S.norm()<E || point.sub(finalPoint).norm()<E)
                break;
            else if(j+1<2)
                    j+=1;
            else{
                j=0;
                S=grad.scale(-1);
            }

        }
        System.out.format("Точка миниммума: (%f , %f)\nf(xmin) = %.16f\n", point.getX1(), point.getX2(), funcValue(f, point));
    }


    public static void minGrad(DoubleBinaryOperator f, Point point) {
        double E = 1e-5;
        Point grad = new Point(dfdx1(f, point), dfdx2(f, point));
        double lambda;
        DoubleUnaryOperator fu;
        while (grad.norm() > E) {
            Point finalPoint = point;
            Point finalGrad = grad;
            fu = (lamb) -> f.applyAsDouble(finalPoint.getX1() - lamb * finalGrad.getX1(), finalPoint.getX2() - lamb * finalGrad.getX2());
            lambda = golden(fu, 0, 1);

            point = point.sub(grad.scale(lambda));
            grad = new Point(dfdx1(f, point), dfdx2(f, point));
            System.out.println(point);
            //System.out.println(grad);

        }
        System.out.format("Точка миниммума: (%f , %f)\nf(xmin) = %.16f\n", point.getX1(), point.getX2(), funcValue(f, point));

    }

    public static void min(DoubleBinaryOperator f, Point[] args) {
        double E = 1e-5;
        Point[] points = args.clone();
        //Arrays.sort(points, (p1, p2) -> Double.compare(f.applyAsDouble(p1.getX1(), p1.getX2()), f.applyAsDouble(p2.getX1(), p2.getX2())));
        Point b;
        Point g;
        Point w;

        double alpha = 1;
        double beta = 0.5;
        double gamma = 2;

        Point xr, xe, xc, c;
        Point mid;
        while (points[0].sub(points[1]).norm() > E || points[0].sub(points[2]).norm() > E || points[1].sub(points[2]).norm() > E) {

            Arrays.sort(points, (p1, p2) -> Double.compare(f.applyAsDouble(p1.getX1(), p1.getX2()), f.applyAsDouble(p2.getX1(), p2.getX2())));
            b = points[0];
            g = points[1];
            w = points[2];
            loop:
            {
                System.out.println(Arrays.toString(points));
                mid = b.add(g).scale(0.5);

                //Отражение
                xr = mid.sub(w).scale(alpha).add(mid);//TODO Проверить

                if (funcValue(f, xr) < funcValue(f, b)) {
                    //Растяжение
                    xe = xr.sub(mid).scale(gamma).add(mid);
                    if (funcValue(f, xe) < funcValue(f, xr))
                        w = xe;
                    else w = xr;
                    break loop;
                }
                if (funcValue(f, b) < funcValue(f, xr) && funcValue(f, xr) < funcValue(f, g)) {
                    w = xr;
                    break loop;
                }
                if (funcValue(f, g) < funcValue(f, xr) && funcValue(f, xr) < funcValue(f, w)) {
                    Point temp = xr;
                    xr = w;
                    w = temp;
                }

                //Сжатие
                xc = w.sub(mid).scale(beta).add(mid);// TODO Проверить
                if (funcValue(f, xc) < funcValue(f, w))
                    w = xc;
                else {
                    g = g.sub(b).scale(0.5).add(b);
                    w = w.sub(b).scale(0.5).add(b);
                }

            }

            points[0] = w;
            points[1] = b;
            points[2] = g;
        }
        Arrays.sort(points, (p1, p2) -> Double.compare(f.applyAsDouble(p1.getX1(), p1.getX2()), f.applyAsDouble(p2.getX1(), p2.getX2())));
        System.out.format("Точка миниммума: (%f , %f)\nf(xmin) = %.16f\n", points[0].getX1(), points[0].getX2(), funcValue(f, points[0]));
    }

    public static void main(String[] args) {
        // write your code here
        //DoubleBinaryOperator f = (x1, x2) -> 100 * (x2 - x1 * x1) * (x2 - x1 * x1) + 5 * (1 - x1) * (1 - x1);
        DoubleBinaryOperator f = (x1, x2) -> (x1 * x1 + x2 - 11) * (x1 * x1 + x2 - 11) + (x1 + x2 * x2 - 7) * (x1 + x2 * x2 - 7);
        Random random = new Random();
        Point[] points = new Point[]{new Point(32, 4), new Point(21, -23), new Point(72, -12)};

        //System.out.println(dfdx1(f,new Point(1.2323124, 3324.3312412)));
        min(f,points);
        // System.out.println(golden((x)->x*x,-2,2));

    }


}
