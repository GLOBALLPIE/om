package com.company;

/**
 * Created by GlobalPie on 19.12.2018.
 */
public class Point {
    private final double x1;
    private final double x2;

    public Point(double x1, double x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public Point add(Point p) {
        return new Point(x1 + p.getX1(), x2 + p.getX2());
    }

    public Point sub(Point p) {
        return new Point(x1 - p.getX1(), x2 - p.getX2());
    }

    public Point scale(double scalar) {
        return new Point(x1 * scalar, x2 * scalar);
    }

    public double norm() {
        return Math.sqrt(x1 * x1 + x2 * x2);
    }
    public Point scaleByLength(){
        return scale(1/norm());
    }

    @Override
    public String toString() {
        return "Point{" +
                "x1=" + x1 +
                ", x2=" + x2 +
                '}';
    }
}
