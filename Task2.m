clc
clear
syms x1 x2;
%f=100*(x2-x1^2)^2+5*(1-x1)^2;
f=(x1^2+x2-11)^2+(x1+x2^2-7)^2;
fg=matlabFunction(f);
dfdx1=matlabFunction(diff(f,x1));
dfdx2=matlabFunction(diff(f,x2));
grad=@(x)[dfdx1(x(1),x(2)),dfdx2(x(1),x(2))];
oldmin=[10 -10];
min=oldmin;
E=10^(-3);
k=0;
j=0;
lambda=0;
syms lamb;

while norm(grad(min))>E
lambda=minFunc2(matlabFunction(subs(f,[x1 x2],min-lamb*grad(min))),0,1);
min=min-lambda*grad(min)
end

fprintf('����� ��������: [%d, %d]\n\n',min);
