clc
clear
syms x1 x2;
%f=100*(x2-x1^2)^2+5*(1-x1)^2;
f=(x1^2+x2-11)^2+(x1+x2^2-7)^2;
fg=matlabFunction(f);
dfdx1=matlabFunction(diff(f,x1));
dfdx2=matlabFunction(diff(f,x2));
grad=@(x)[dfdx1(x(1),x(2)),dfdx2(x(1),x(2))];
oldmin=[0 0];
minimum=oldmin;
S=-grad(oldmin);
E=10^(-5);
k=0;
j=0;
lambda=0;
result=minimum;
syms lamb;

while 1 
lambda=minFunc2(matlabFunction(subs(f,[x1 x2],oldmin+lamb*S)),0,1);
minimum=oldmin+lambda*S
w=(norm(grad(minimum))^2)/(norm(grad(oldmin))^2);
S=-grad(minimum)+w*S;
if (norm(S)<E) || (norm(minimum-oldmin)<E)
    result=minimum;
    break
else
    if j+1<2
        j=j+1;
        oldmin=minimum;
    else
        oldmin=minimum;
        k=k+1;
        j=0;
        S=-grad(oldmin);
    end
end
end

fprintf('����� ��������: [%d, %d]\n\n',result);

