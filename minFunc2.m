function z=minFunc2(f,a,b)                           
epsilon=0.000001;
iter= 50;
tau=double((sqrt(5)-1)/2);
k=0;
x1=a+(1-tau)*(b-a);
x2=a+tau*(b-a);
f_x1=f(x1);
f_x2=f(x2);
while ((abs(b-a)>epsilon) && (k<iter))
    k=k+1;
    if(f_x1<f_x2)
        b=x2;
        x2=x1;
        x1=a+(1-tau)*(b-a);
        
        f_x1=f(x1);
        f_x2=f(x2);
    else
        a=x1;
        x1=x2;
        x2=a+tau*(b-a);
        
        f_x1=f(x1);
        f_x2=f(x2);
    end
    
    k=k+1;
end
% chooses minimum point
if(f_x1<f_x2)
    z=x1;
else
    z=x2;
end
end